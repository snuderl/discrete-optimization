#!/usr/bin/python
# -*- coding: utf-8 -*-

from ortools.constraint_solver import pywrapcp

"httpciteseerx.ist.psu.eduviewdocdownloaddoi=10.1.1.53.5405&rep=rep1&type=pdf"

def dfs(graph, n):
    visited = set()
    start = [0]
    counts = [-1] * n
    counts[0] = 0
    while start:
        node = start[0]
        start = start[1:]

        for x in range(n):
            if graph[node, x] == True and x not in visited:
                counts[x] = counts[node] + 1
                visited.add(x)
                start = start + [x]
    return counts

def maxNeighbours(graph, n):
    return int(max([sum(graph[x, :]) for x in range(n)]))

def findCliques(graph, n):
    import networkx as nx
    import networkx.algorithms.approximation as ap
    G=nx.Graph()
    for x in range(n):
        for y in range(n):
            if graph[x, y]:
                G.add_edge(x, y)

    cliques = list(ap.max_clique(G))
    print cliques
    return cliques
    for x in cliques:
        print "Max clique found", len(x)
        return x




def csp(input_data):
    # Modify this code to run your optimization algorithm
    # parse the input
    lines = input_data.split('\n')


    first_line = lines[0].split()
    node_count = int(first_line[0])
    edge_count = int(first_line[1])

    import numpy as np
    graph = np.zeros((node_count, node_count), np.bool)
    colors = np.zeros((node_count, ), np.int8)
    colors[:] = -1

    edges = []
    for i in range(1, edge_count + 1):
        line = lines[i]
        parts = line.split()
        v1, v2 = int(parts[0]), int(parts[1])
        edges.append(())
        graph[v1, v2] = True
        graph[v2, v1] = True

    #clique = findCliques(graph, node_count)

    #print dfs(graph, node_count)

    deg = maxNeighbours(graph, node_count)
    print "Degree, ", deg

    solver = pywrapcp.Solver('Coloring')   
    #vs = []
    # for x in range(0, node_count):
    #     if x in clique:
    #         ind = clique.index(x)
    #         vs.append(solver.IntVar(ind, ind, "v(%i)"%x))
    #     else:
    #         vs.append(solver.IntVar(0, 123, "v(%i)"%x))


    vs = [solver.IntVar(0, 0)] + [solver.IntVar(0, 16, "v(%i)"%i) for i in range(1 ,node_count)]
    for v1 in range(node_count):
        for v2 in range(node_count): 
            if(graph[v1, v2]):
                #print v1, v2
                solver.Add(vs[v1] != vs[v2])

    #for cl in clique:
    #solver.AllDifferent([vs[x] for x in clique])

    ##Symetry constraints
    for x in range(1, node_count):
        m = solver.Max(vs[:x])
        solver.Add(vs[x] <= (m + 1))

    db = solver.Phase(vs,
                      solver.ASSIGN_MIN_VALUE,
                      solver.CHOOSE_FIRST_UNBOUND)

    solver.Solve(db)

    solver.NewSearch(db)
    if solver.NextSolution():
        solution = [vs[i].Value() for i in range(node_count)]
        colorsNum = max(solution) + 1
        output_data = str(colorsNum) + ' ' + str(0) + '\n'
        output_data += ' '.join(map(str, solution))

        print solution
        print solver
        return output_data

def solve_it(input_data):
    return csp(input_data)
    # Modify this code to run your optimization algorithm

    # parse the input
    lines = input_data.split('\n')

    first_line = lines[0].split()
    node_count = int(first_line[0])
    edge_count = int(first_line[1])

    import numpy as np
    graph = np.zeros((node_count, node_count), np.bool)
    colors = np.zeros((node_count, ), np.int8)
    colors[:] = -1

    edges = []
    for i in range(1, edge_count + 1):
        line = lines[i]
        parts = line.split()
        v1, v2 = int(parts[0]), int(parts[1])
        edges.append(())
        graph[v1, v2] = True
        graph[v2, v1] = True


    #print graph

    # build a trivial solution
    # every node has its own color
    #solution = range(0, node_count)

    maxColor = 1
    colors[0] = 0

    # toDo = [x for x in range(1, node_count)]

    # print "abc", toDo
    # while len(toDo) > 0:
    #     #print toDo
    #     completed = False
    #     for x in list(toDo):
    #         print x
    #         color = colors[graph[x, :]]
    #         posible = set(range(0, maxColor + 1)) - {z for z in color}
    #         #print color, posible
    #         if posible:
    #             print x, color, posible, graph[x, :]
    #             colors[x] = min(posible)
    #             completed = True
    #             toDo.remove(x)
    #             completed = True
    #             break

    #     if not completed:
    #         print "forcing"
    #         colors[toDo[0]] = maxColor
    #         maxColor += 1
    #         completed = False
    #         #print toDo, x
    #         toDo.remove(toDo[0])

    maxNumberOfEdges = max([sum(graph[x, :]) for x in range(node_count)])


    for x in range(node_count):
      color = colors[graph[x, :]]
      posible = set(range(0, maxColor + 1)) - {z for z in color}
      #print color, posible
      if posible:
          colors[x] = min(posible)
      else:
          colors[x] = maxColor
      maxColor += 1


    colorsNum = len({x for x in colors})
    solution = colors

    if maxNumberOfEdges < colorsNum:
        print "Worse than upper bound of number of edges."

    # prepare the solution in the specified output format

    output_data = str(colorsNum) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, solution))

    print output_data

    return output_data


import sys

if __name__ == '__main__':
    if not len(sys.argv) > 1:
        file_location = "data/gc_70_7"
    else:
        file_location = sys.argv[1].strip()

    input_data_file = open(file_location, 'r')
    input_data = ''.join(input_data_file.readlines())
    input_data_file.close()
    print solve_it(input_data)