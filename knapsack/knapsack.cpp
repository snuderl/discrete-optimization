#include <unordered_map>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <vector>
#include <cmath>
#include <algorithm> 


using namespace std;

unordered_map<unsigned long, unsigned long> store;
int counts;



struct Item{
	unsigned int weight;
	unsigned int value;
	unsigned int position;
};

bool sorter(Item const& a, Item const& b)
{
	return a.weight > b.weight;
}

bool sorterPosition(Item const& a, Item const& b)
{
	return a.position < b.position;
}

int min(int i1, int i2){
	return i1 < i2 ? i1 : i2;
}

int max(int i1, int i2){
	return i1 > i2 ? i1 : i2;
}


Item *items;

unsigned long dp(unsigned long k, int j){
	//cout << k << " " << j << endl;
	unsigned long code = k * counts + j;

	auto pointer = store.find(code);
	if (pointer != store.end()) return pointer->second;

	unsigned long val;

	if (j == 0){
		store[code] = 0;
		return 0;
	}

	Item next = items[j - 1];

	if (next.weight <= k){
		unsigned long v1 = dp(k, j - 1);
		unsigned long v2 = dp(k - next.weight, j - 1) + next.value;
		val = v1 > v2 ? v1 : v2;
		store[code] = val;
		return val;
	}
	else{
		val = dp(k, j - 1);
		store[code] = val;
		return val;
	}

}

struct Pair{
	unsigned int capacity;
	unsigned int value;
};

void dpTable2(int cap){
	vector<Pair> *table = new vector<Pair>[counts];

	unsigned int *lastTable = new unsigned int[cap + 1];
	unsigned int *currentTable = new unsigned int[cap + 1];

	Item item = items[0];

	for (int w = 0; w < cap + 1; w++){
		lastTable[w] = 0;
	}

	//cout << counts << endl;
	for (int i = 0; i < counts; i++){
		//cout << "Procesing item: " << i << endl;
		item = items[i];
		Pair abc;
		abc.capacity = 0;
		abc.value = 0;
		table[i].push_back(abc);

		for (int w = 0; w < cap + 1; w++){
			if (item.weight <= w){
				currentTable[w] = max(lastTable[w], lastTable[w - item.weight] + item.value);
			}
			else{
				currentTable[w] = lastTable[w];
			}
			if (currentTable[w] != currentTable[w - 1]){
				Pair p;
				p.capacity = w;
				p.value = currentTable[w];
				table[i].push_back(p);
			}
		}

		for (int w = 0; w < cap + 1; w++){
			//cout << currentTable[w] << ", ";
			lastTable[w] = currentTable[w];
		}
		//cout << endl;
	}


	for (int i = 0; i < cap + 1; i++){
		currentTable[i] = 0;
	}

	//cout << "backtracking" << endl;

	short *output = new short[counts];
	Pair current = table[counts - 1].back();
	int value = current.value;
	int i = counts - 2;

	cout << current.value << " " << 1 << endl;

	//cout << current.capacity << " " << current.value << endl;
	while (i >= 0){
		int k = cap;
		auto pointer = table[i].end() - 1;
		while (pointer != table[i].begin() - 1){
			Pair p = *pointer;
			if (p.capacity <= current.capacity){
				if (p.value == current.value){
					output[items[i + 1].position] = 0;
					current = p;
				}
				else{
					output[items[i + 1].position] = 1;
					while (p.capacity != current.capacity - items[i + 1].weight && p.capacity > 0){
						pointer--;
						p = *pointer;
						//cout << p.capacity << endl;
					}
					current = p;
				}
				//cout << "Backtracking " << p.capacity << " " << p.value << endl;
				break;
			}
			pointer--;
		}
		i--;
	}

	if (items[0].weight <= current.capacity){
		output[items[0].position] = 1;
	}
	else{
		output[items[0].position] = 0;
	}

	int s = 0;
	int capacity = 0;
	for (int i = 0; i < counts; i++){
		int pos = items[i].position;
		if (output[pos] == 1){
			s += items[i].value;
			capacity += items[i].weight;
		}
		cout << output[i] << " ";
	}





	cout << endl;
	//cout << s << endl;
	//cout << capacity << " " << cap << " " << (capacity <= cap) << endl;
}

void dpTable(int cap){
	unsigned int **t = new unsigned int*[counts];
	t[0] = new unsigned int[cap + 1];
	for (int w = 0; w < cap + 1; w++){
		t[0][w] = 0;
	}
	Item item = items[0];

	for (int i = 0; i < counts; i++){
		//cout << "Procesing item: " << i << endl;
		unsigned int *currentTable = new unsigned int[cap + 1];
		unsigned int *lastTable = t[i];

		item = items[i];
		for (int w = 0; w < cap + 1; w++){
			if (item.weight <= w){
				currentTable[w] = max(lastTable[w], lastTable[w - item.weight] + item.value);
			}
			else{
				currentTable[w] = lastTable[w];
			}
		}

		t[i + 1] = currentTable;
	}

	cout << t[counts][cap] << " " << 0 << endl;

	int *output = new int[counts];
	int w = cap;
	int i = counts;
	while (i > 0){
		if (t[i][w] == t[i - 1][w]){
			output[i - 1] = 0;
		}
		else{
			output[i - 1] = 1;
			w -= items[i - 1].weight;
		}
		i--;
	}

	int s = 0;
	int capacity = 0;
	for (int i = 0; i < counts; i++){
		if (output[i] == 1){
			s += items[i].value;
			capacity += items[i].value;
		}
		cout << output[i] << " ";
	}

	cout << endl;
	//cout << s << endl;
	cout << capacity << " " << cap << " " << (capacity < cap) << endl;

	cout << endl;
	//cout << w << endl;
}

int main(){

	long capacity;
	scanf("%d %d", &counts, &capacity);
	//cout << count << " " << capacity << endl;

	double minimizer = 1.0;
	capacity = capacity;
	items = new Item[counts];
	for (int i = 0; i < counts; i++){
		Item item;
		scanf("%d %d", &item.value, &item.weight);
		item.weight = item.weight;
		item.position = i;
		items[i] = item;
		//dp(capacity, i);
	}

	sort(items, items + counts, &sorter);

	// for(int i = 0; i < count; i++){

	// 	Item item = items[i];
	// 	cout << item.value << ", " << item.weight << endl;
	// }


	clock_t tStart = clock();
	dpTable2(capacity);
	//dpTable(capacity);
	//cout << dp(capacity, count) << endl;
	//printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
	return 0;
}

