#!/usr/bin/python
# -*- coding: utf-8 -*-

from collections import namedtuple
Item = namedtuple("Item", ['index', 'value', 'weight'])
Node = namedtuple("Node", ['value', 'room', 'estimate', "indices", "index", "took"])

store = []
def solve_it(input_data):
    # Modify this code to run your optimization algorithm
    global capacity, store
    # parse the input
    lines = input_data.split('\n')

    firstLine = lines[0].split()
    item_count = int(firstLine[0])
    capacity = int(firstLine[1])

    print "Capacity", capacity

    items = []

    for i in range(1, item_count+1):
        line = lines[i]
        parts = line.split()
        items.append(Item(i-1, int(parts[0]), int(parts[1])))
    import sys
    sys.setrecursionlimit(1500)


    # a trivial greedy algorithm for filling the knapsack
    # it takes items in-order until the knapsack is full
    value = 0
    weight = 0
    taken = [0]*len(items)

    for item in items:
        if weight + item.weight <= capacity:
            taken[item.index] = 1
            value += item.value
            weight += item.weight

    #value, taken = branchAndBound(items, capacity)
    print taken, value
    store = {}
    print len(store)
    from time import time
    start = time()
    value = dp(items, store, capacity, len(items))
    print "Time taken: ", time() - start
    start = time()
    #dpNonRecursive(items, capacity)
    print "Time taken: ", time() - start

    ###We have to backtrack on store to get the selected items
    #print store
    itemInd = len(items)
    val = value
    k = capacity
    taken = []
    for x in range(capacity):
        if store.get((itemInd, x)) == val:
            k = x
            break
    while itemInd > 0:
        #print val
        if store.get((itemInd -1, k)) == val:
            taken.append(0)
        else:
            taken.append(1)
            val -= items[itemInd - 1].value
            k -= items[itemInd - 1].weight
        itemInd -= 1
    taken = taken[::-1]

    print val, k

    
    # prepare the solution in the specified output format
    output_data = str(value) + ' ' + str(0) + '\n'
    output_data += ' '.join(map(str, taken))
    return output_data

def memoize(fun):
    def inner(items, k, j):
        global store
        val = store.get((j, k), None)
        if not val == None:
            return val
        else:
            val = fun(items, k, j)
            store[(j, k)] = val
            return val

    return inner

def dp(items, store, k, j):
    v = store.get((j, k), None)
    if not v == None:
        return v

    if(j == 0): 
        store[(j, k)] = 0
        return 0
    if(items[j - 1].weight <= k): 
        val = max(dp(items, store, k, j-1), dp(items, store, k - items[j - 1].weight, j - 1) + items[j - 1].value)
        store[(j, k)] = val
        return val 
    store[(j, k)] = dp(items, store, k, j-1)
    return store[(j, k)]

def dpNonRecursive(items, capacity):
    j = len(items)

    from collections import deque
    store = {}

    stack = deque()
    stack.append((capacity, len(items), 0))

    val = 0
    while stack:
        k, j, stage = stack.pop()
        val = store.get((k, j), None)
        if not val == None:
            continue

        if j == 0:
            store[j] = 0
            continue

        if items[j - 1].weight <= k:
            if stage == 0:
                stack.append((k, j, 1))
                stack.append((k, j - 1, 0))
                stack.append((k - items[j - 1].weight, j-1, 0))
            elif stage == 1:
                v1, v2 = 0, 0
                if j > 1:
                    v1 = store[k * capacity + j - 1]
                    v2 = store[(k - items[j - 1].weight) * capacity + j - 1]
                val = max(v1, v2 + items[j - 1].value)
                store[k * capacity + j] = val
        else:
            if stage==0:
                stack.append((k, j, stage + 1))
                stack.append((k, j-1, 0))
            elif stage == 1:
                v = 0
                if(j > 1):
                    v = store[k * capacity + j - 1]
                store[k * capacity + j] = v
                val = v
            else:
                print "error"

    print "value", val




def branchAndBound(items, K):
    from collections import deque
    import numpy as np
    values = np.array(map(lambda x: x.value, items))
    weights = np.array(map(lambda x: x.weight, items))

    ratios = np.array([values[i] / float(weights[i]) for i in range(len(values))])
    indices = np.argsort(ratios)[::-1]
    ratios = ratios[indices]
    values = values[indices]
    weights = weights[indices]

    print values, weights, ratios

    bestValue = 0
    bestNode = None

    def calculateRelaxation(indices, capacity, startingInd):
        filled = sum([values[x] for x in indices])
        relaxation = 0

        for i in range(startingInd, len(items)):
            if(capacity <= 0):
                break

            if(i in indices): continue

            w = min(weights[i], capacity)
            relaxation += values[i] * float(w) / weights[i]
            capacity -= w            

        #print indices, filled + relaxation, filled
        return filled + relaxation



    stack = deque()
    node = Node(calculateRelaxation({}, K, 0), K, sum(values), [], -1, None)
    #print node
    stack.append(node)

    while stack:
        node = stack.pop()
        index = node.index + 1
        #print node

        if(index == len(items)):
            continue

        if(node.estimate < bestValue):
            continue

        indices = node.indices + [index]
        weight = node.room - weights[index]
        n1 = Node(calculateRelaxation(indices, weight, index + 1), weight, node.estimate, indices, index, 1)
        if(n1.room >= 0 and n1.estimate >= bestValue):
            stack.append(n1)
            if(n1.value >= bestValue and index == len(items) - 1):
                bestValue = n1.value
                bestNode = n1

        n2 = Node(calculateRelaxation(node.indices, node.room, index + 1), node.room, node.estimate - values[index], node.indices, index, 0)
        if(n2.room >= 0 and n2.estimate >= bestValue):
            stack.append(n2)
            if(n2.value >= bestValue and index == len(items) - 1):
                bestValue = n2.value
                bestNode = n2

    #print bestNode
    chosen = np.zeros((len(items)), np.int8)
    chosen[bestNode.indices] = 1


    return bestValue, chosen.tolist()



import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        file_location = sys.argv[1].strip()
        input_data_file = open(file_location, 'r')
        input_data = ''.join(input_data_file.readlines())
        input_data_file.close()
        print solve_it(input_data)
    else:
        print 'This test requires an input file.  Please select one from the data directory. (i.e. python solver.py ./data/ks_4_0)'

